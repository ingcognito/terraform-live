// Configure the Google Cloud provider

variable "project_name" {
  type = string
}

variable "project_id" {
  type = string
}

variable "gcp_credentials_path" {
  type = string
}

variable "region" {
  type = string
}

variable "zone" {
  type = string
}

variable "app_name" {
  type = string
}

variable "private_subnet_cidr_1" {
  type = string
}

variable "lb_static_ip" {
  description = "loadbalancer static ip"
}
