terraform {
  required_version = ">= 1.1.0"
  // backend "gcs" {
  //   credentials = "./terraform-sa-keyfile.json"
  //   bucket      = "terraform-remote-state-ingcogito"
  //   prefix      = "terraform/state"
  // }
}

provider "google" {
  project     = var.project_name
  credentials = file("terraform-sa-keyfile.json")
  region      = var.region
  zone        = var.zone
  version     = "~> 4.15"
}

provider "google-beta" {
  project     = var.project_name
  credentials = file("terraform-sa-keyfile.json")
  region      = var.region
  zone        = var.zone
  version     = "~> 4.15"
}

data "google_client_config" "current" {}

data "google_project" "project" {
  project_id = var.project_id
}