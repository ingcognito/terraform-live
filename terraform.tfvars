project_name          = "homelab"
project_id            = "homelab-266501"
app_name              = "ingcognito"
gcp_credentials_path  = "./terraform-sa-keyfile.json"
region                = "northamerica-northeast1"
zone                  = "northamerica-northeast1-a"
private_subnet_cidr_1 = "10.10.1.0/24"

lb_static_ip = "34.107.157.79"
